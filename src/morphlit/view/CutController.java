package morphlit.view;

import java.util.LinkedList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;


public class CutController{
	@FXML
	private Canvas imageCanvas;
	 
	@FXML
	private BorderPane parentPane;
	
	@FXML
	private ChoiceBox<String> cbSizes;
	
	GraphicsContext gc;
	
	Image image;
	private double startX;
	private double startY;
	
	private double frameX=0;
	private double frameY=0;
	private double frameWidth;
	private double frameHeight;
	private double boundsX;
	private double boundsY;
	
	Controller callingController;
	private boolean sourceNotTarget;
	
	ObservableList<String> dataListe = null;
	
	public void init(Image image,Controller callingController, boolean sourceNotTarget) {
		gc=imageCanvas.getGraphicsContext2D();
				
		this.callingController=callingController;
		this.sourceNotTarget=sourceNotTarget;
		
		this.image=image;
		stageImage();
		
		imageCanvas.setOnMousePressed(canvasOnMousePressedEventHandler);
        imageCanvas.setOnMouseDragged(canvasOnMouseDraggedEventHandler);
        draw();
	}
	
	private void stageImage() {
		
		imageCanvas.setWidth(image.getWidth());
		imageCanvas.setHeight(image.getHeight());
		
		List<String> imageSizes=new LinkedList<String>();
		
		//init Choice Box
		if(image.getHeight()>32 &&image.getWidth()>32) imageSizes.add("32x32");
		if(image.getHeight()>64 &&image.getWidth()>64) imageSizes.add("64x64");
		if(image.getHeight()>128 &&image.getWidth()>128) imageSizes.add("128x128");
		if(image.getHeight()>256 &&image.getWidth()>256) imageSizes.add("256x256");
		if(image.getHeight()>512 &&image.getWidth()>512) imageSizes.add("512x512");
		if(image.getHeight()>1024 &&image.getWidth()>1024) imageSizes.add("1024x1024");
		
		cbSizes.setItems(FXCollections.observableArrayList(imageSizes));
		
		//Choice Box Changed
		cbSizes.valueProperty().addListener((observable, oldValue, newValue) -> {
			frameX=0;
			frameY=0;
			frameWidth=Double.parseDouble(newValue.split("x")[0]);
			frameHeight=Double.parseDouble(newValue.split("x")[1]);
			draw();
		});
		
		cbSizes.getSelectionModel().clearAndSelect(0);
		
		parentPane.getScene().getWindow().sizeToScene();
		
		Bounds bounds=imageCanvas.localToScene(imageCanvas.getBoundsInLocal());
		boundsX=bounds.getMinX();
		boundsY=bounds.getMinY();
	}
	
	 EventHandler<MouseEvent> canvasOnMousePressedEventHandler = new EventHandler<MouseEvent>()
	    {
	        @Override
	        public void handle(MouseEvent mouseEvent)
	        {
	        	startX = mouseEvent.getSceneX();
	        	startY = mouseEvent.getSceneY();
	        	
	        	
	        	Bounds bounds=imageCanvas.localToScene(imageCanvas.getBoundsInLocal());
	        	boundsX=bounds.getMinX();
	    		boundsY=bounds.getMinY();
	        	
	        	/*System.out.println("startX: "+startX);
	        	System.out.println("startY: "+startY);
	        	System.out.println("BoundsX:"+bounds.getMaxX()+" "+bounds.getMinX());
	        	System.out.println("BoundsY:"+bounds.getMaxY()+ " "+bounds.getMinY());*/

	        }
	    };

	    EventHandler<MouseEvent> canvasOnMouseDraggedEventHandler = new EventHandler<MouseEvent>()
	    {
	        @Override
	        public void handle(MouseEvent mouseEvent)
	        {
	            double offsetX = mouseEvent.getSceneX() - startX;
	            double offsetY = mouseEvent.getSceneY() - startY;
	            
	            frameX=startX+offsetX-boundsX;
	            frameY=startY+offsetY-boundsY;
	            
	            //catch if Frame would go off the canvas
	            if(frameX<0) frameX=0;
	            if(frameX+frameWidth>imageCanvas.getWidth())frameX=imageCanvas.getWidth()-frameWidth;
	            if(frameY<0) frameY=0;
	            if(frameY+frameHeight>imageCanvas.getHeight())frameY=imageCanvas.getHeight()-frameHeight;
	            
	            draw();
	        }
	    };
	    
	    
	    private void draw() {
	    	//image background
	    	gc.drawImage(image, 0,0);
	    	
	    	//Selection Frame
	    	gc.setStroke(Color.BLACK);
	    	gc.strokeRect(frameX,frameY , frameWidth, frameHeight);
	    	gc.strokeLine(frameX + frameWidth/2, frameY, frameX+frameWidth/2, frameY+frameHeight);
	    	gc.strokeLine(frameX , frameY + frameHeight/2, frameX+frameWidth, frameY+frameHeight/2);
	    	
	    	
	    	//Grey overlap
	    	gc.setFill(Color.rgb(100, 100, 100, 0.4));
	    	gc.fillRect(0,0,imageCanvas.getWidth(),frameY);
	    	gc.fillRect(0,frameY,frameX,imageCanvas.getHeight());
	    	gc.fillRect(frameX,frameY+frameHeight,imageCanvas.getWidth(),imageCanvas.getHeight());
	    	gc.fillRect(frameX+frameWidth, frameY, imageCanvas.getWidth(),frameHeight);

	    	
	    	
	    }
	    
	    @FXML
	    private void onclickCut() {
	    	//Picture too small to select
	    	if(image.getWidth()<32 || image.getHeight()<32)
	    	{
	    		closeWindow();
	    	}else {
		    	image.getPixelReader().getArgb(0, 0);
		    	WritableImage newImage= new WritableImage((int)frameWidth, (int)frameHeight);
		    	newImage.getPixelWriter().setPixels(
		    		0,	//Destination x
		    		0,  //Destination y
		    		(int)frameWidth, //Copy width
		    		(int)frameHeight,  //Copy height
		    		image.getPixelReader(), //Source Reader
		    		(int)frameX,  //Source x
		    		(int)frameY  //Source y
		    	);
		    	
		    	if(sourceNotTarget) {
		    		callingController.setSourceImage(newImage, (int)frameWidth +"x"+ (int)frameHeight);
		    	}else {
		    		callingController.setTargetImage(newImage, (int)frameWidth +"x"+ (int)frameHeight);
		    	}
	    	}
	    	
	    	((Stage)parentPane.getScene().getWindow()).close();
	    }

	    
	    public void closeWindow() {
	    	if(sourceNotTarget) {
	    		callingController.setSourceImage(null, "");
	    	}else {
	    		callingController.setTargetImage(null, "");
	    	}
	    }
}
