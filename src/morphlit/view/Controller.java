package morphlit.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import morphlit.logic.AdjustableModel;
import morphlit.logic.GifCreator;
import morphlit.logic.LogicModel;
import morphlit.logic.MorphController;

public class Controller {

	@FXML
	private ImageView imgViewSrc;
	@FXML
	private ImageView imgViewTrg;
	@FXML
	private Pane pMain;
	@FXML
	private TabPane tabPane;
	@FXML
	private VBox vBox1;
	@FXML
	private TextField textSrc;
	@FXML
	private TextField textTrg;
	@FXML
	private ChoiceBox<String> cbOrder;
	@FXML
	private ChoiceBox<String> cbColor;
	@FXML
	private ChoiceBox<String> cbTrend;
	@FXML
	private ChoiceBox<String> cbIntensity;
	@FXML
	private Slider sliderSpeed;
	@FXML
	private Label labelSpeed;
	@FXML
	private Label lblSrcSize;
	@FXML
	private Label lblTrgSize;
	@FXML
	private Button btnCreateAnimation;
	@FXML
	private ImageView imgViewPreview;
	@FXML
	private Button btnSetTrgAsSrc;
	@FXML
	private Button btnSaveGif;
	
	
	private LogicModel logicModel;

	private String srcSize="";
	private String trgSize="";

	private GifCreator gifCreator;


	public Controller() {
		logicModel = new LogicModel(); 
	}

	@FXML
	private void initialize() {
		// set initial view (logo and and name) visible, everything else invisible
		tabPane.setVisible(false);
		vBox1.setVisible(true);
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE); // tabs cant be closed
		btnSetTrgAsSrc.setVisible(false);
		btnSaveGif.setVisible(false);

		// fill Choice Boxes with items
		cbColor.setItems(FXCollections.observableArrayList(logicModel.getColorDataFactoryNames()));
		cbOrder.setItems(FXCollections.observableArrayList(logicModel.getMorphOrderNames()));
		cbTrend.setItems(FXCollections.observableArrayList(logicModel.getProgressModelNames()));
		cbIntensity.setItems(FXCollections.observableArrayList(logicModel.getIntensityModelNames()));
		
		// initialize ChoiceBoxes with first choice pre-selected
		cbColor.getSelectionModel().select(0);
		cbOrder.getSelectionModel().select(0);
		cbTrend.getSelectionModel().select(0);
		cbIntensity.getSelectionModel().select(0);
		
		
		// Listeners for ChoiceBox selection changes
		cbColor.valueProperty().addListener((observable, oldValue, newValue) -> {
			logicModel.selectColorDataFactory(newValue);
			showSettingsDialog(logicModel.getColorDataFactory());
		});
		cbOrder.valueProperty().addListener((observable, oldValue, newValue) -> {
			logicModel.selectMorphOrder(newValue);
			showSettingsDialog(logicModel.getMorphOrder());
		});
		cbTrend.valueProperty().addListener((observable, oldValue, newValue) -> {
			logicModel.selectProgressModel(newValue);
			showSettingsDialog(logicModel.getProgressModel());
		});
		cbIntensity.valueProperty().addListener((observable, oldValue, newValue) -> {
			logicModel.selectIntensityModel(newValue);
			showSettingsDialog(logicModel.getIntensityModel());
		});
		
		gifCreator = new GifCreator();
	}
	
	// Select a new source image
    @FXML 
    private void onclickSource() {
    	FileChooser fc = new FileChooser();
        fc.setTitle("Choose Resource File");
        File fileSrc = fc.showOpenDialog(null);
        try {
        	if (Files.probeContentType(fileSrc.toPath()).startsWith("image")) {  // file-type check
				openCutDialog( new Image(new FileInputStream(fileSrc.getAbsolutePath())),true);
		    
				Main.imageSrc = new Image(new FileInputStream(fileSrc.getAbsolutePath()));
				textSrc.setText(fileSrc.getAbsolutePath());
		    }else
				textSrc.setText("Invalid Image selected (Invalid MIME-Type)");	    
		} catch (IllegalArgumentException | NullPointerException | IOException e ) {
        	System.out.println(e + " Image could not be loaded!");
		}
	}
    
	// Select a new target image
    @FXML private void onclickTarget() {
    	FileChooser fc = new FileChooser();
        fc.setTitle("Choose Target File");
        File fileTrg = fc.showOpenDialog(null);
        try {
		    if (Files.probeContentType(fileTrg.toPath()).startsWith("image")) {
				openCutDialog( new Image(new FileInputStream(fileTrg.getAbsolutePath())), false);
				textTrg.setText(fileTrg.getAbsolutePath());
		    }else
				textTrg.setText("Invalid Image selected (Invalid MIME-Type)");	    
		} catch (IllegalArgumentException | NullPointerException | IOException e ) {
        	System.out.println(e + " Image could not be loaded!");
		}
	}
    
	// Start the morphing process
    @FXML private void createAnimationClicked() {
    	if(Main.imageSrc != null && Main.imageTrg != null && (Main.imageSrc.getWidth()<= Main.imageTrg.getWidth() && Main.imageSrc.getHeight()<= Main.imageTrg.getHeight())) {
			btnSaveGif.setVisible(false);
			btnSetTrgAsSrc.setVisible(false);
			btnCreateAnimation.setVisible(false);
	    	MorphController c = new MorphController(Main.imageSrc, Main.imageTrg, logicModel);
	    	c.setOnCreateFrameListener(e -> {
				Image image = c.getImage();
				gifCreator.addImage(SwingFXUtils.fromFXImage(image, null));
				Platform.runLater(() -> imgViewPreview.setImage(image));
			});
	    	c.init();
	    	
	    	Thread t = new Thread(() -> {
				while(c.step());
				btnSaveGif.setVisible(true);
				btnSetTrgAsSrc.setVisible(true);
				btnCreateAnimation.setVisible(true);
			});
	    	t.setDaemon(true);
	    	t.start();
    	} else { // if source or target image it not set jump back to image selection
			tabPane.getSelectionModel().select(0);
    	}
    }
    
	// Set the result image as new source image
    @FXML 
    private void onclickTargetAsSource() {
    	if(imgViewPreview.getImage() != null) 
    	{
    		try {
    			Main.imageSrc = imgViewPreview.getImage();
    			tabPane.getSelectionModel().select(0);
    			textSrc.setText("The previous result image is now set as source image!");
    		} catch (IllegalArgumentException | NullPointerException e ) {
            	System.out.println(e + " Image could not be set!");
    		}
    	}
	}
    
    // Open a generic properties dialogue for the various settings
	private void showSettingsDialog(AdjustableModel model) {
		if(!model.hasParameters())
			return;
		Stage newStage = new Stage();
		BorderPane borderPane = new BorderPane();
		VBox comp = new VBox();
		borderPane.setCenter(comp);
		Label top = new Label("Parameters");
		top.setFont(Font.font(Font.getDefault().getFamily(), 25));
		top.setPrefHeight(30);
		top.setPrefWidth(200);
		top.setAlignment(Pos.CENTER);
		top.setPadding(new Insets(0,0,10,0));
		borderPane.setTop(top);
		
		// add CheckBoxes with Listener for every Boolean parameter 
		for(String n : model.getBooleanParameters()) {
			CheckBox c = new CheckBox(n);
			c.setPadding(new Insets(3));
			c.selectedProperty().set((boolean)model.getParameter(n).getValue());
			c.selectedProperty().addListener((observable, oldValue, newValue) -> model.getParameter(n).setValue(newValue));
			comp.getChildren().add(c);
		}
		
		// add TextFields with Listener for every Double parameter 
		for(String n : model.getDoubleParameters()) {
			TextField tf = new TextField(model.getParameter(n).getValue().toString());
			tf.setPrefWidth(100);
			tf.setPadding(new Insets(3));
			tf.focusedProperty().addListener((observable, oldValue, newValue) -> {
				try {
					double v = Double.parseDouble(tf.getText());
					model.getParameter(n).setValue(v);
				}catch(NumberFormatException e) {
					tf.setText(model.getParameter(n).getValue().toString());
				}
			});
			Label lbl = new Label(n);
			lbl.setPadding(new Insets(3));
			lbl.setPrefWidth(100);
			HBox hbox = new HBox(lbl, tf);
			hbox.setAlignment(Pos.CENTER);
			comp.getChildren().add(hbox);
		}
		
		Button btnClose = new Button("Close");
		btnClose.setPrefWidth(200);
		btnClose.setPrefHeight(30);
		btnClose.setOnAction(event -> newStage.close());
		borderPane.setBottom(btnClose);

		Scene stageScene = new Scene(borderPane);
		newStage.setScene(stageScene);
		newStage.initModality(Modality.APPLICATION_MODAL);
		newStage.show();
	}
	
	// Start Button to show the Main-UI and set the start-screen invisible
	@FXML
	private void onclickStart() {
		tabPane.setVisible(true);
		vBox1.setVisible(false);
	}

	@FXML
	private void changeSpeed() {
		labelSpeed.setText("Speed: " + (int) sliderSpeed.getValue());
	}
	
	private void openCutDialog(Image image, boolean sourceNotTarget) {
		final FXMLLoader loader = new FXMLLoader(getClass().getResource("CutUI.fxml"));
		Parent root=null;
		try {
			root = loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		CutController controller = loader.getController();
		Stage stage = new Stage();
		stage.setTitle("Morphlit select Image");
		Scene scene=new Scene(root);
		scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		stage.setScene(scene);
		stage.show();
		
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	          public void handle(WindowEvent we) {
	        	  controller.closeWindow();
	          }
		});
		
		controller.init(image, this, sourceNotTarget);
	}
	
	public void setSourceImage(Image image, String size) {
		Main.imageSrc=image;
		if(image==null) { // if cut Dialog was closed without select
			textSrc.setText("");
			srcSize="";
			lblSrcSize.setText(srcSize);
			lblTrgSize.setText(trgSize);
			return;
		}
		
		srcSize=size;
		if(trgSize!=null && trgSize.trim().length()>0 && !trgSize.contains(size)) {
			lblSrcSize.setText(srcSize+ " (Target and Source size not matching)");
			lblTrgSize.setText(trgSize+ " (Target and Source size not matching)");
		}
		else {
			lblSrcSize.setText(srcSize);
			lblTrgSize.setText(trgSize);
		}
	}
	
	public void setTargetImage(Image image, String size) {
		Main.imageTrg=image;
		if(image==null) { // if cut Dialog was closed without select
			textTrg.setText("");
			trgSize="";
			lblSrcSize.setText(srcSize);
			lblTrgSize.setText(trgSize);
			return;
		}
		
		trgSize=size;
		if(srcSize!=null && srcSize.trim().length()>0 && !srcSize.contains(size)) {
			lblSrcSize.setText(srcSize+ " (Target and Source size not matching)");
			lblTrgSize.setText(trgSize+ " (Target and Source size not matching)");
		}
		else {
			lblSrcSize.setText(srcSize);
			lblTrgSize.setText(trgSize);
		}
	}
	
	
	// Show dialogue with correspoding properties when properties icon is clicked
	@FXML
	private void onColorModelSettingsClicked() {
		showSettingsDialog(logicModel.getColorDataFactory());
	}
	
	@FXML
	private void onOrderModelSettingsClicked() {
		showSettingsDialog(logicModel.getMorphOrder());
	}
	
	@FXML
	private void onIntensityModelSettingsClicked() {
		showSettingsDialog(logicModel.getIntensityModel());
	}
	
	@FXML
	private void onProgressModelSettingsClicked() {
		showSettingsDialog(logicModel.getProgressModel());
	}
	
	// Save created GIF
	@FXML
	private void saveGif() {
		FileChooser fc = new FileChooser();
        fc.setTitle("Save Animation");
        File target = fc.showSaveDialog(null);
        gifCreator.saveGif(target);
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setContentText("");
        alert.setHeaderText("Your animation was saved!");
        alert.showAndWait();
	}
}
