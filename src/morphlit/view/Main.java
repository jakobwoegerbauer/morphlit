package morphlit.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {
	static Image imageSrc;
	static Image imageTrg;
	static Scene scene;
 

	// Set Scene to our mainUI.fxml and show the GUI
	@Override
	public void start(Stage primaryStage) throws Exception {
        Pane root = (Pane)FXMLLoader.load((getClass().getResource("mainUI.fxml")));
        scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Morphlit");
        primaryStage.show();
        
	}

	public static void main(String[] args) {
		launch(args);		
	}
}