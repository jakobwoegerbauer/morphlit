package morphlit.logic;

import java.util.Arrays;

import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

public class ImageQuad {
	
	private ImageQuad parent;
	private WritableImage image;
	private int x;
	private int y;
	private int width;
	private int height;
	private ImageQuad[] children;
	private boolean isMorphed;
	
	private ColorData hierarchicalAverage;
	
	public static ImageQuad createImageTree(WritableImage image, ColorDataFactory cFactory) {
		return new ImageQuad(image, 0, 0, (int)image.getWidth(), (int)image.getHeight(), cFactory, null);
	}
	
	private ImageQuad(WritableImage image, int x, int y, int width, int height, ColorDataFactory cFactory, ImageQuad parent) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		isMorphed = false;
		this.parent = parent;
		
		if(this.width == 1 || this.height == 1) {
			calculateHierarchicalAverage(cFactory);
			return;
		}
		
		children = new ImageQuad[4];
		children[0] = new ImageQuad(image, x, y, width/2, height/2, cFactory, this);
		children[1] = new ImageQuad(image, x+width/2, y, width/2, height/2, cFactory, this);
		children[2] = new ImageQuad(image, x, y+height/2, width/2, height/2, cFactory, this);
		children[3] = new ImageQuad(image, x+width/2, y+height/2, width/2, height/2, cFactory, this);
		calculateHierarchicalAverage(cFactory);
	}
	
	public ColorData getHierarchicalAverageColor(ColorDataFactory cFactory) {
		return hierarchicalAverage;
	}
	
	public ColorData getAverageColor(ColorDataFactory cFactory) {
		ColorData sum = null;
		PixelReader r = image.getPixelReader();
		for(int i = x; i < x+width; i++) {
			for(int j = y; j < y+height; j++) {
				if(sum == null)
					sum = cFactory.createColorData(r.getColor(i, j));
				else
					sum = sum.add(cFactory.createColorData(r.getColor(i, j)));
			}
		}
		return sum.multiply(1./(width*height));
	}
	
	public void morph(ColorData diff, ColorDataFactory cFactory) {
		morph_impl(diff, cFactory);
		isMorphed = true;
		updateAveragesUpwards(this.getParent(), cFactory);
	}
	
	private void morph_impl(ColorData diff, ColorDataFactory cFactory) {
		if(children == null) {
			PixelReader r = image.getPixelReader();
			PixelWriter p = image.getPixelWriter();
			for(int i = x; i < x+width; i++) {
				for(int j = y; j < y+height; j++) {
					p.setColor(i, j, cFactory.createColorData(r.getColor(i, j)).add(diff).toColor());
				}
			}
			calculateHierarchicalAverage(cFactory);
			return;
		}
		
		for(int i = 0; i < 4; i++) {
			children[i].morph_impl(diff, cFactory);
		}
		calculateHierarchicalAverage(cFactory);
	}
	
	private static void updateAveragesUpwards(ImageQuad q, ColorDataFactory cFactory) {
		if(q == null)
			return;
		q.calculateHierarchicalAverage(cFactory);
		updateAveragesUpwards(q.getParent(), cFactory);		
	}
	
	private void calculateHierarchicalAverage(ColorDataFactory cFactory) {
		if(children == null) {
			hierarchicalAverage = getAverageColor(cFactory);
			return;
		}
		hierarchicalAverage = cFactory.getZero();
		for(int i = 0; i < 4; i++) {			
			hierarchicalAverage = hierarchicalAverage.add(children[i].getHierarchicalAverageColor(cFactory));
		}
		hierarchicalAverage = hierarchicalAverage.multiply(1./4.);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public ImageQuad[] getChildren() {
		if(children == null)
			return null;
		return Arrays.copyOf(children, 4);
	}
	
	public WritableImage getImage() {
		return new WritableImage(image.getPixelReader(), (int)image.getWidth(), (int)image.getHeight());
	}
	
	public boolean isMorphed() {
		return isMorphed;
	}

	public ImageQuad getParent() {
		return parent;
	}

	public void removeChildren() {
		children = null;		
	}
}
