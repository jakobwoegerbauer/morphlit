package morphlit.logic;

import java.awt.event.ActionListener;

import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;

public class MorphController {
	private ColorData[][] destPrefixSum;
	private ImageQuad currentImage;
	private ImageQuad lastMorphed;
	private ActionListener onCreateFrameListener;
	
	private ColorDataFactory colorDataFactory;
	private MorphOrder morphOrder;
	private ProgressModel progressModel;
	private IntensityModel intensityModel;
	private boolean initialized;
	private Image lastFrame;
	
	public MorphController(Image src, Image dest, LogicModel logicModel) {
		WritableImage i = new WritableImage(src.getPixelReader(), (int)src.getWidth(), (int)src.getHeight());
		this.colorDataFactory = logicModel.getColorDataFactory();
		this.morphOrder = logicModel.getMorphOrder();
		this.progressModel = logicModel.getProgressModel();
		this.intensityModel = logicModel.getIntensityModel();
		
		currentImage = ImageQuad.createImageTree(i, colorDataFactory);
		createDestImagePrefixSum(dest);
	}

	public void init(){
		morphOrder.init();
		progressModel.init();
		lastFrame = currentImage.getImage();
		initialized = true;
	}
	
	public void setOnCreateFrameListener(ActionListener onCreateFrameListener) {
		this.onCreateFrameListener = onCreateFrameListener;
	}
	
	public boolean step() {
		if(!initialized)
			throw new UnsupportedOperationException("init() has to be called before the first step");

		if(progressModel.getStepCount() == 0){
			notifyOnCreateFrameListener();
		}
		
		lastMorphed = morphOrder.getNextMorphStepInfo(currentImage, lastMorphed);
		if(lastMorphed == null){
			notifyOnCreateFrameListener();
			initialized = false;
			return false;
		}
		
		ColorData avgDestColor = getDestColorDataSum(lastMorphed.getX(), lastMorphed.getY(), lastMorphed.getWidth(), lastMorphed.getHeight())
				.multiply(1./(lastMorphed.getWidth()*lastMorphed.getHeight()));
		double intensity = intensityModel.getIntensity(lastMorphed.getWidth(), currentImage.getWidth(), progressModel.getStepCount());
		ColorData diff = avgDestColor.add(lastMorphed.getHierarchicalAverageColor(colorDataFactory).multiply(-1.)).multiply(intensity);
		
		lastMorphed.morph(diff, colorDataFactory);
		if(progressModel.countStep()) {
			notifyOnCreateFrameListener();
		}
		return true;
	}
	
	private void notifyOnCreateFrameListener() {
		lastFrame = currentImage.getImage();
		if(onCreateFrameListener != null)
			onCreateFrameListener.actionPerformed(null);
	}
	
	public Image getImage() {
		return lastFrame;
	}
	
	private void createDestImagePrefixSum(Image dest) {
		int width = (int)dest.getWidth();
		int height = (int)dest.getHeight();
		destPrefixSum = new ColorData[width][height];
		PixelReader pReader = dest.getPixelReader();
		
		destPrefixSum[0][0] = colorDataFactory.createColorData(pReader.getColor(0, 0));
		for(int i = 1; i < height; i++) {
			destPrefixSum[0][i] = destPrefixSum[0][i-1].add(colorDataFactory.createColorData(pReader.getColor(0, i)));  
		}
		for(int i = 1; i < width; i++) {
			destPrefixSum[i][0] = destPrefixSum[i-1][0].add(colorDataFactory.createColorData(pReader.getColor(i, 0)));
		}
		for(int i = 1; i < width; i++) {
			for(int j = 1; j < height; j++) {
				destPrefixSum[i][j] = destPrefixSum[i-1][j].add(destPrefixSum[i][j-1]).add(destPrefixSum[i-1][j-1].multiply(-1)).add(colorDataFactory.createColorData(pReader.getColor(i, j)));
			}
		}
	}
	
	private ColorData getDestColorDataSum(int x, int y, int width, int height) {
		if(x == 0 && y == 0)
			return destPrefixSum[width-1][height-1];
		
		if(x == 0)
			return destPrefixSum[x+width-1][y+height-1]
					.add(destPrefixSum[x+width-1][y-1].multiply(-1));
		
		if(y == 0)
			return destPrefixSum[x+width-1][y+height-1]
					.add(destPrefixSum[x-1][y+height-1].multiply(-1));
		
		return destPrefixSum[x+width-1][y+height-1]
				.add(destPrefixSum[x+width-1][y-1].multiply(-1))
				.add(destPrefixSum[x-1][y+height-1].multiply(-1))
				.add(destPrefixSum[x-1][y-1]);
	}
}
