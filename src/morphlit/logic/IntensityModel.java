package morphlit.logic;

public abstract class IntensityModel extends AdjustableModel {
	public abstract double getIntensity(int quadSize, int imageWidth, int stepNr);
}
