package morphlit.logic;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import morphlit.logic.impl.BreadthFirstOrder;
import morphlit.logic.impl.ConstantIntensityModel;
import morphlit.logic.impl.DepthFirstOrder;
import morphlit.logic.impl.HSBColorDataFactory;
import morphlit.logic.impl.LinearProgressModel;
import morphlit.logic.impl.LogarithmicIntensityModel;
import morphlit.logic.impl.RGBColorDataFactory;
import morphlit.logic.impl.RandomIntensityModel;
import morphlit.logic.impl.RandomMorphOrder;
import morphlit.logic.impl.RandomTopDownOrder;
import morphlit.logic.impl.ExponentialProgressModel;
import morphlit.logic.impl.YCbCrColorDataFactory;

public class LogicModel {
	
	private Map<String, ColorDataFactory> colorDataFactories;
	private Map<String, MorphOrder> morphOrders;
	private Map<String, ProgressModel> progressModels;
	private Map<String, IntensityModel> intensityModels;
	
	private ColorDataFactory colorDataFactory;
	private MorphOrder morphOrder;
	private ProgressModel progressModel;
	private IntensityModel intensityModel;
	
	public LogicModel() {
		colorDataFactories = new HashMap<String, ColorDataFactory>();
		colorDataFactories.put("RGB", new RGBColorDataFactory());
		colorDataFactories.put("YCbCr", new YCbCrColorDataFactory());
		colorDataFactories.put("HSB", new HSBColorDataFactory());
		
		morphOrders = new HashMap<String, MorphOrder>();
		morphOrders.put("Breadth First Order", new BreadthFirstOrder());
		morphOrders.put("Depth First Order", new DepthFirstOrder());
		morphOrders.put("Random Top-down Order", new RandomTopDownOrder());
		morphOrders.put("Random Order", new RandomMorphOrder());
		
		progressModels = new HashMap<String, ProgressModel>();
		progressModels.put("Linear", new LinearProgressModel());
		progressModels.put("Exponential", new ExponentialProgressModel());
		
		intensityModels = new HashMap<String, IntensityModel>();
		intensityModels.put("Logarithmic", new LogarithmicIntensityModel());
		intensityModels.put("Constant", new ConstantIntensityModel());
		intensityModels.put("Random", new RandomIntensityModel());
		
		colorDataFactory = colorDataFactories.values().iterator().next();
		morphOrder = morphOrders.values().iterator().next();
		progressModel = progressModels.values().iterator().next();
		intensityModel = intensityModels.values().iterator().next();
	}
	
	public Collection<String> getColorDataFactoryNames() {
		return colorDataFactories.keySet();
	}
	
	public void selectColorDataFactory(String name) {
		colorDataFactory = colorDataFactories.get(name);
	}
	
	public Collection<String> getMorphOrderNames() {
		return morphOrders.keySet();
	}
	
	public void selectMorphOrder(String name) {
		morphOrder = morphOrders.get(name);
	}
	
	public Collection<String> getProgressModelNames() {
		return progressModels.keySet();
	}
	
	public void selectProgressModel(String name) {
		progressModel = progressModels.get(name);
	}
	
	public Collection<String> getIntensityModelNames() {
		return intensityModels.keySet();
	}
	
	public void selectIntensityModel(String name) {
		intensityModel = intensityModels.get(name);
	}

	public ColorDataFactory getColorDataFactory() {
		return colorDataFactory;
	}

	public MorphOrder getMorphOrder() {
		return morphOrder;
	}

	public ProgressModel getProgressModel() {
		return progressModel;
	}

	public IntensityModel getIntensityModel() {
		return intensityModel;
	}
}
