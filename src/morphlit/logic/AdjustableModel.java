package morphlit.logic;

import java.util.Collection;
import java.util.LinkedList;
import java.util.stream.Collectors;

public abstract class AdjustableModel {

	private Collection<Parameter> parameters;

	public AdjustableModel() {
		parameters = new LinkedList<Parameter>();
	}

	protected void addParameter(Parameter parameter) {
		parameters.add(parameter);
	}

	public boolean hasParameters() {
		return !parameters.isEmpty();
	}

	public Collection<String> getBooleanParameters() {
		return getParameters(Boolean.class);
	}

	public Collection<String> getDoubleParameters() {
		return getParameters(Double.class);
	}

	private Collection<String> getParameters(Class<?> cls) {
		return parameters.stream().filter(p -> p.getDefaultValue().getClass() == cls).map(p -> p.getName())
				.collect(Collectors.toList());
	}

	public Parameter getParameter(String name) {
		return parameters.stream().filter(p -> p.getName() == name).findFirst().orElse(null);
	}
}
