package morphlit.logic;

import javafx.scene.paint.Color;

public abstract class ColorDataFactory extends AdjustableModel {
	
	public abstract ColorData createColorData(Color color);

	public abstract ColorData getZero();
}
