package morphlit.logic;

import static javax.imageio.ImageIO.getImageWritersBySuffix;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.IIOImage;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

public class GifCreator {

	private ImageWriter imageWriter;
	private FileImageOutputStream outputStream;
	private File tempFile;
	private boolean writing;

	public GifCreator() {
		writing = false;
		init();
	}

	private void init() {
		try {
			if (tempFile != null && tempFile.exists())
				tempFile.delete();
			if (writing) {
				imageWriter.endWriteSequence();
				outputStream.close();
			}

			imageWriter = getImageWritersBySuffix("gif").next();

			tempFile = File.createTempFile("_result", ".gif.tmp");
			tempFile.deleteOnExit();
			outputStream = new FileImageOutputStream(tempFile);
			imageWriter.setOutput(outputStream);
			imageWriter.prepareWriteSequence(null);
			writing = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Appends a frame to the current gif
	 * @param image frame to add
	 */
	public void addImage(BufferedImage image) {
		try {
			if (!writing) {
				init();
			}
			imageWriter.writeToSequence(new IIOImage(image, null, null), null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Copies the created tempfile to the given destination.
	 * @param file destination of gif file
	 */
	public void saveGif(File file) {
		try {
			if (writing) {
				imageWriter.endWriteSequence();
				outputStream.flush();
				outputStream.close();
				writing = false;
			}
			if (file.exists())
				file.delete();
			Files.copy(tempFile.toPath(), file.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
