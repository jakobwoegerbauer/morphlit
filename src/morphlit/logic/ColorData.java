package morphlit.logic;

import javafx.scene.paint.Color;

public interface ColorData {
	ColorData add(ColorData other);
	ColorData multiply(double factor);
	Color toColor();
}
