package morphlit.logic;

public abstract class MorphOrder extends AdjustableModel {	
	public abstract ImageQuad getNextMorphStepInfo(ImageQuad image, ImageQuad last);

	public void init(){

	}
}