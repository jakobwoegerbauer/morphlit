package morphlit.logic;

public abstract class ProgressModel extends AdjustableModel {

	private int stepCnt;
	private int frameCnt;

	public boolean countStep(){
		stepCnt++;
		if(shouldCreateFrame()){
			frameCnt++;
			return true;
		}
		return false;
	}

	public int getStepCount(){
		return stepCnt;
	}

	public int getFrameCount(){
		return frameCnt;
	}

	public void init() {
		stepCnt = 0;
		frameCnt = 0;
	}

	public abstract boolean shouldCreateFrame();
}
