package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;
import morphlit.logic.ColorDataFactory;
import morphlit.logic.Parameter;

public class RGBColorDataFactory extends ColorDataFactory {

	private Parameter cRed, cGreen, cBlue, cAlpha;

	public RGBColorDataFactory() {
		cRed = new Parameter("Change Red", "", true);
		cGreen = new Parameter("Change Green", "", true);
		cBlue = new Parameter("Change Blue", "", true);
		cAlpha = new Parameter("Change Alpha", "", true);

		super.addParameter(cRed);
		super.addParameter(cGreen);
		super.addParameter(cBlue);
		super.addParameter(cAlpha);
	}

	@Override
	public ColorData createColorData(Color color) {
		return new RGBColorData(color.getRed(), color.getGreen(), color.getBlue(), color.getOpacity(),
				(boolean) cRed.getValue(), (boolean) cGreen.getValue(), (boolean) cBlue.getValue(),
				(boolean) cAlpha.getValue());
	}

	@Override
	public ColorData getZero() {
		return new RGBColorData(0, 0, 0, 0,
				(boolean) cRed.getValue(), (boolean) cGreen.getValue(), (boolean) cBlue.getValue(),
				(boolean) cAlpha.getValue());
	}
}
