package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;
import morphlit.logic.ColorDataFactory;
import morphlit.logic.Parameter;

public class HSBColorDataFactory extends ColorDataFactory {

	private Parameter cHue, cSat, cBright;
	
	public HSBColorDataFactory() {
		cHue = new Parameter("Change Hue", "", true);
		cSat = new Parameter("Change Saturation", "", true);
		cBright = new Parameter("Change Brightness", "", true);
		
		super.addParameter(cHue);
		super.addParameter(cSat);
		super.addParameter(cBright);
	}

	@Override
	public ColorData createColorData(Color color) {
		return new HSBColorData(color.getHue(), color.getSaturation(), color.getBrightness(),
				(boolean) cHue.getValue(), (boolean) cSat.getValue(), (boolean) cBright.getValue());
	}

	@Override
	public ColorData getZero() {
		return new HSBColorData(0, 0, 0, (boolean) cHue.getValue(), (boolean) cSat.getValue(), (boolean) cBright.getValue());
	}
}
