package morphlit.logic.impl;

import java.util.Random;

import morphlit.logic.ImageQuad;

public class RandomMorphOrder extends RandomTopDownOrder {

	private Random rand;
	private int nextIndex;
	private int lastIndex;
	private ImageQuad[] currentQuads;

	@Override
	public void init() {
		super.init();
		rand = new Random();
		nextIndex = 0;
	}

	@Override
	public ImageQuad getNextMorphStepInfo(ImageQuad image, ImageQuad last) {
		if(last == null) {
			currentQuads = new ImageQuad[3*image.getWidth()*image.getHeight()];
			return image;
		}
		
		ImageQuad[] c = last.getChildren();
		if(c != null) {
			currentQuads[lastIndex] = c[0];
			for(int i = 1; i < 4; i++) {
				currentQuads[nextIndex] = c[i];
				nextIndex++;
			}
		}else {
			currentQuads[lastIndex] = null;
		}
		lastIndex = rand.nextInt(nextIndex);
		if(currentQuads[lastIndex] == null)
			return selectImageQuad(image);
		return currentQuads[lastIndex];
	}
}

