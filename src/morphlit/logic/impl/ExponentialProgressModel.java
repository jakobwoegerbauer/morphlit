package morphlit.logic.impl;

import morphlit.logic.Parameter;
import morphlit.logic.ProgressModel;

public class ExponentialProgressModel extends ProgressModel {

	private Parameter exponentialBase;
	private int iterationCnt;
	private double iterationMax;

	public ExponentialProgressModel() {
		exponentialBase = new Parameter("Exponential Base", "", 1.5);
		addParameter(exponentialBase);
	}

	@Override
	public boolean countStep() {
		iterationCnt++;
		if (iterationCnt >= iterationMax) {
			iterationCnt = 0;
			iterationMax *= (double)exponentialBase.getValue();
		}
		return super.countStep();
	}

	@Override
	public boolean shouldCreateFrame() {
		return iterationCnt == 0;
	}

	@Override
	public void init() {
		iterationMax = 1;
		iterationCnt = 0;
	}
}
