package morphlit.logic.impl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;

import morphlit.logic.ImageQuad;
import morphlit.logic.MorphOrder;

public class BreadthFirstOrder extends MorphOrder {

	private Queue<ImageQuad> queue = new LinkedList<ImageQuad>();
	
	@Override
	public ImageQuad getNextMorphStepInfo(ImageQuad image, ImageQuad last) {
		if(last == null)
			return image;
		ImageQuad[] c = last.getChildren();
		if(c != null)
			queue.addAll(Arrays.stream(c).collect(Collectors.toList()));
		return queue.poll();
	}
}
