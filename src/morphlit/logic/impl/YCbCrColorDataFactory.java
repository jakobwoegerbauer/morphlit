package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;
import morphlit.logic.ColorDataFactory;

public class YCbCrColorDataFactory extends ColorDataFactory {

	@Override
	public ColorData createColorData(Color color) {
		double r = color.getRed();
		double g = color.getGreen();
		double b = color.getBlue();
		double y = 0.299 * r + 0.587 * g + 0.114 * b;
		double cb = -0.169 * r - 0.331 * g + 0.500 * b;
		double cr = 0.500 * r - 0.419 * g - 0.081 * b;
		return new YCbCrColorData(y, cb, cr);
	}

	@Override
	public ColorData getZero() {
		return new YCbCrColorData(0, 0, 0);
	}

}
