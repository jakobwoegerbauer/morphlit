package morphlit.logic.impl;

import morphlit.logic.ImageQuad;
import morphlit.logic.MorphOrder;

public class DepthFirstOrder extends MorphOrder {

	@Override
	public ImageQuad getNextMorphStepInfo(ImageQuad image, ImageQuad last) {
		if (last == null)
			return image;
		return search(image);
	}

	private ImageQuad search(ImageQuad q) {
		if (q == null)
			return null;
		if (!q.isMorphed())
			return q;
		ImageQuad[] c = q.getChildren();
		if (c == null) {
			return null;
		}
		for (int i = 0; i < 4; i++) {
			ImageQuad n = search(c[i]);
			if (n != null)
				return n;
		}
		return null;
	}
}
