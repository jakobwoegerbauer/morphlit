package morphlit.logic.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import morphlit.logic.ImageQuad;
import morphlit.logic.MorphOrder;

public class RandomTopDownOrder extends MorphOrder {

	private Random rand;
	private List<Integer> indizes;
	
	public RandomTopDownOrder() {
		rand = new Random();
		indizes = new LinkedList<Integer>();
		indizes.add(0);
		indizes.add(1);
		indizes.add(2);
		indizes.add(3);
	}
	
	@Override
	public ImageQuad getNextMorphStepInfo(ImageQuad image, ImageQuad last) {
		return selectImageQuad(image);
	}
	
	protected ImageQuad selectImageQuad(ImageQuad q) {
		if(q == null || !q.isMorphed())
			return q;

		ImageQuad[] c = q.getChildren();
		if(c == null)
			return null;
		
		int[] order = getOrder();
		ImageQuad n = null;
		for(int i = 0; i < 4; i++) {
			n = selectImageQuad(c[order[i]]);
			if(n != null)
				return n;
		}
		q.removeChildren();
		return null;
	}
	
	
	private int[] getOrder() {
		int[] r = new int[4];
		List<Integer> values = new LinkedList<Integer>(indizes);
		for(int i = 0; i < 4; i++) {
			int x = rand.nextInt(values.size());
			r[i] = values.remove(x);
		}
		return r;
	}
}
