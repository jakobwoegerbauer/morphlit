package morphlit.logic.impl;

import morphlit.logic.IntensityModel;
import morphlit.logic.Parameter;

public class ConstantIntensityModel extends IntensityModel {

	public ConstantIntensityModel() {
		super.addParameter(new Parameter("Intensity", "", 1.));
	}
	
	@Override
	public double getIntensity(int quadSize, int imageWidth, int stepNr) {
		return (double)getParameter("Intensity").getValue();
	}
}
