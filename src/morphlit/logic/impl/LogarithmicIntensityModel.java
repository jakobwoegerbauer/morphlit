package morphlit.logic.impl;

import morphlit.logic.IntensityModel;

public class LogarithmicIntensityModel extends IntensityModel {
	
	@Override
	public double getIntensity(int quadSize, int imageWidth, int stepNr) {
		return 1./log(quadSize+1, 2);
	}
	
	private double log(double x, double b) {
		return Math.log(x)/Math.log(b);
	}
}
