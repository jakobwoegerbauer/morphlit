package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;

public class RGBColorData implements ColorData {

	private double r, g, b, a;
	private boolean cr, cg, cb, ca;

	public RGBColorData(double r, double g, double b, double a, boolean cr, boolean cg, boolean cb, boolean ca) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
		this.cr = cr;
		this.cg = cg;
		this.cb = cb;
		this.ca = ca;
	}

	@Override
	public ColorData add(ColorData other) {
		RGBColorData d = (RGBColorData) other;
		return new RGBColorData(cr ? r + d.getR() : r, cg ? g + d.getG() : g, cb ? b + d.getB() : b, ca ? a + d.getA() : a, cr, cg, cb, ca);
	}

	@Override
	public ColorData multiply(double factor) {
		return new RGBColorData(cr ? r * factor : r, cg ? g * factor : g, cb ? b * factor : b, ca ? a * factor : a, cr, cg, cb, ca);
	}

	@Override
	public Color toColor() {
		return new Color(clip(r), clip(g), clip(b), clip(a));
	}

	private double clip(double v) {
		return Math.min(1, Math.max(0, v));
	}

	public double getR() {
		return r;
	}

	public double getG() {
		return g;
	}

	public double getB() {
		return b;
	}

	public double getA() {
		return a;
	}
}
