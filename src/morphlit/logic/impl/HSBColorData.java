package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;

public class HSBColorData implements ColorData {

	private double hue, saturation, brightness;
	private boolean cHue, cSat, cBright;
	
	public HSBColorData(double hue, double saturation, double brightness, boolean cHue, boolean cSat, boolean cBright) {
		this.hue = hue;
		this.saturation = saturation;
		this.brightness = brightness;
		this.cHue = cHue;
		this.cSat = cSat;
		this.cBright = cBright;
	}

	@Override
	public ColorData add(ColorData other) {
		HSBColorData c = (HSBColorData)other;
		return new HSBColorData(cHue ? hue + c.getHue() : hue, cSat ? saturation + c.getSaturation() : saturation, cBright ? brightness + c.getBrightness() : brightness,
				cHue, cSat, cBright);
	}

	@Override
	public ColorData multiply(double factor) {
		return new HSBColorData(cHue ? hue * factor : hue, cSat ? saturation * factor : saturation, cBright ? brightness * factor : brightness,
				cHue, cSat, cBright);
	}

	@Override
	public Color toColor() {
		return Color.hsb(clip(hue, 360), clip(saturation), clip(brightness));
	}
	
	private double clip(double v) {
		return Math.min(1, Math.max(0, v));
	}
	
	private double clip(double v, double max) {
		return Math.min(max, Math.max(0, v));
	}

	public double getHue() {
		return hue;
	}

	public double getSaturation() {
		return saturation;
	}

	public double getBrightness() {
		return brightness;
	}
}
