package morphlit.logic.impl;

import morphlit.logic.Parameter;
import morphlit.logic.ProgressModel;

public class LinearProgressModel extends ProgressModel {
	private Parameter stepsPerFrame;

	public LinearProgressModel() {
		stepsPerFrame = new Parameter("Steps per Frame", "", 100000.0);
		super.addParameter(stepsPerFrame);
	}

	@Override
	public boolean shouldCreateFrame() {
		return getStepCount() % (int)(double)stepsPerFrame.getValue() == 0;
	}
}
