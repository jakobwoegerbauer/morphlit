package morphlit.logic.impl;

import java.util.Random;

import morphlit.logic.IntensityModel;

public class RandomIntensityModel extends IntensityModel {

	private Random rand;
	
	public RandomIntensityModel() {
		rand = new Random();
	}
	
	@Override
	public double getIntensity(int quadSize, int imageWidth, int stepNr) {
		return rand.nextDouble();
	}

}
