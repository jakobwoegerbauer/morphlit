package morphlit.logic.impl;

import javafx.scene.paint.Color;
import morphlit.logic.ColorData;

public class YCbCrColorData implements ColorData {

	private double y, cb, cr;
	
	public YCbCrColorData(double y, double cb, double cr) {
		this.y = y;
		this.cb = cb;
		this.cr = cr;
	}
	
	@Override
	public ColorData add(ColorData other) {
		YCbCrColorData c = (YCbCrColorData)other;
		return new YCbCrColorData(y + c.getY(), cb + c.getCb(), cr + c.getCr());
	}

	@Override
	public ColorData multiply(double factor) {
		return new YCbCrColorData(y * factor, cb * factor, cr * factor);
	}

	@Override
	public Color toColor() {
		 double r = y + 0.000 * cb + 1.403 * cr;
         double g = y - 0.344 * cb - 0.714 * cr;
         double b = y + 1.773 * cb + 0.000 * cr;
         return new Color(clip(r), clip(g), clip(b), 1);
	}
	
	private double clip(double v) {
		return Math.min(1, Math.max(0, v));
	}

	public double getY() {
		return y;
	}

	public double getCb() {
		return cb;
	}

	public double getCr() {
		return cr;
	}
}
