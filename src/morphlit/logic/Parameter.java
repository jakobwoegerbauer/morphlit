package morphlit.logic;

public class Parameter {
	private String name;
	private String description;
	private Object value;
	private Object defaultValue;
	
	public Parameter(String name, String description, Object defaultValue) {
		this.name = name;
		this.description = description;
		this.value = this.defaultValue = defaultValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object intensity) {
		this.value = intensity;
	}

	public Object getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
}
